import React from "react";
import { useState,useEffect } from "react";

function MainPage() {
    const [posts,setPosts] = useState([])

    const getPosts = async () => {
      const url = 'http://localhost:8000/api/posts'
      const response = await fetch(url);
      if(response.ok) {
        const data = await response.json();
        setPosts(data);
      }
    }
    useEffect(() => {
      getPosts();
    },[]);

return (
        <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">neddit</h1>
        <div className="col-lg-6 mx-auto">
            <p className="lead mb-4">neddit</p>
        <div>
            {posts.map((post) => (
            <div className="card" key={post.id}>
                <img className="card-img-top" src={post.image_url || post.image_file}></img>
                <div className="card-body">
                <h5 className="card-title">{post.title}</h5>
                <p className="card-text">{post.description}</p>
                <a href="#" className="btn btn-primary">
                    Go somewhere
                </a>
                </div>
            </div>
            ))}
        </div>
        </div>
    </div>
    )
 }

export default MainPage;
