from django.contrib import admin
from .models import CustomUser
admin.site.register(CustomUser)

class UserAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "first_name",
        "last_name",
        "email"
    )
