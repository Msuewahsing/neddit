from django.shortcuts import render
from rest_framework import generics
from .serializers import PostsSerializer, CustomUserSerializer
from .models import Posts
from django.http import JsonResponse

def Posts_List(request):
    if request.method == 'GET':
        posts = Posts.objects.all()
        print(posts)
        serializer = PostsSerializer(posts, many=True)
        return JsonResponse(serializer.data, safe=False)
