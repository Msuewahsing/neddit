from django.db import models



class Posts(models.Model):
    image_file = models.ImageField(upload_to='images/', blank=True, null=True)
    image_url = models.URLField(blank=True, null=True)
    title = models.CharField(max_length=80)
    description = models.TextField(max_length=255)
    user_id = models.ForeignKey(
        'users.CustomUser',
        on_delete=models.CASCADE,
        related_name='posts'
    )

    def __str__(self):
        return f"{self.user_id.user_name} - {self.title}"
